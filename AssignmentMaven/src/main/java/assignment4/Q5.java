package assignment4;

import java.util.Random;

public class Q5 {

	public static void main(String[] args) {
		int arr[] = new int[10];
        int largest = 0;
        int secondLargest = 0;
        
        Random r = new Random();
        
        System.out.println("Random array are:");
        for(int x=0;x<arr.length;x++) {
        	arr[x]=r.nextInt(100);//generate random number with range 0-99
        	System.out.print(arr[x]+"  ");
        }
        
        
        for (int i = 0; i < arr.length; i++)
        {
            if (arr[i] > largest)
            {
                secondLargest = largest;
                largest = arr[i];
            }
            else if (arr[i] > secondLargest)
            {
                secondLargest = arr[i];
            }
        }
        System.out.println("\nLargest Number is: "  +largest);
        System.out.println("Second largest number is: " + secondLargest);

	}

}
