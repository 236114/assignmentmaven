package assignment4;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Q3 {

	public static void main(String[] args) {
		
		List<String> car=new ArrayList<String>();
		
		car.add("BMW");
		car.add("Volvo");
		car.add("Nissan");
		
		System.out.println("ArrayList size = "+car.size());
		
		Iterator<String> i = car.iterator();
		System.out.println("While loop:");
		while(i.hasNext()) {
			System.out.println(i.next());
		}
		
        System.out.println("Advanced for loop:");
        for(Object obj : car) {//for each or advance for loop use for array....for(dataType item : array)
            System.out.println(obj);
    }
		
		List<String> list = new ArrayList<String>(car);
		
		System.out.println("For loop:");
		for(int x=0;x<list.size();x++){
			System.out.println(list.get(x));
		}
		
		
		
	}
}
