package assignment4;

import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		
		int x,y,temp;

		Scanner s = new Scanner(System.in);
		System.out.print("Enter value for x:");
		x=s.nextInt();
		System.out.print("Enter value of y: ");
		y=s.nextInt();
		
		System.out.println("Before.....X="+x+"  Y="+y);
		temp=x;
		x=y;
		y=temp;
		System.out.println("After.....X="+x+"  Y="+y+"  Temp="+temp);
		
		

	}

}
