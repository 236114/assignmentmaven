package assignment4;

import java.util.Scanner;

public class Q2 {

	public static void main(String[] args) {
		
		int temp, num;
		boolean isPrime = true;
		
		Scanner s = new Scanner(System.in);
		System.out.print("Enter num value:");
		num = s.nextInt();
		
		for(int x=2; x<=(num/2); x++) {
			
			temp=num%x;
			if(temp==0) {
				isPrime = false;
				break;
			}
		}
		
		if(isPrime) {
			System.out.println(num+" is a prime number");
		}else {
			System.out.println(num+" is NOT a prime number");
		}
		
	}

}

